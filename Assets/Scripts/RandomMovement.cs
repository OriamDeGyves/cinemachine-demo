﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Playables;

public class RandomMovement : MonoBehaviour
{
    public Collider walkableArea;
    public float timeToRetarget = 3.0f;
    public float minDistance = 10.0f;
    public PlayableDirector switchToAerealCamera;
    public PlayableDirector switchToFollowCamera;

    private Bounds bounds;
    private NavMeshAgent _agent;

    private bool isFollowCamera = true;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        bounds = walkableArea.bounds;
    }

    private IEnumerator Start()
    {
        float randomPositionX = 0.0f;
        float randomPositionZ = 0.0f;
        Vector3 randomPosition;

        while (true)
        {
            do
            {
                float t1 = Random.Range(0.0f, 1.0f);
                randomPositionX = t1 * bounds.min.x + (1.0f - t1) * bounds.max.x;

                float t2 = Random.Range(0.0f, 1.0f);
                randomPositionZ = t2 * bounds.min.z + (1.0f - t2) * bounds.max.z;

                randomPosition = new Vector3(randomPositionX, transform.position.y, randomPositionZ);

            } while (Vector3.Distance(randomPosition, transform.position) <= minDistance);

            _agent.SetDestination(randomPosition);

            yield return new WaitForSeconds(timeToRetarget);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isFollowCamera)
            {
                switchToAerealCamera.Stop();
                switchToAerealCamera.Play();
            }
            else
            {
                switchToFollowCamera.Stop();
                switchToFollowCamera.Play();
            }

            isFollowCamera = !isFollowCamera;
        }
    }
}